// * @Author: Ismail Debele Asega
// * @Email: asega03@gmail.com
// * @LinkedIn: https://www.linkedin.com/in/asegaismail/
// * @Github: https://github.com/ismailasega
// * @GitLab: https://gitlab.com/asegaismail
// * @Tel: +256-784-491412 / +256-756-454376

import React from 'react';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import UploadDocuments from '../components/documentUpload'
import Divider from '@mui/material/Divider';

const UploadViewPage = () => {
    return (
        < >
            <Container maxWidth='lg' sx={{ marginTop: 5, display: 'flex', flexDirection: 'column', }}>
                <Typography variant='h5'>
                    KYC Documents
                </Typography>
                <Divider />
                <UploadDocuments />
            </Container>
        </>
    );
}

export default UploadViewPage;