// * @Author: Ismail Debele Asega
// * @Email: asega03@gmail.com
// * @LinkedIn: https://www.linkedin.com/in/asegaismail/
// * @Github: https://github.com/ismailasega
// * @GitLab: https://gitlab.com/asegaismail
// * @Tel: +256-784-491412 / +256-756-454376

export default {
     // If accessing local backend api that is not containeraized
    // API_URL: 'http://localhost:8081/'

    // If accessing local backend api that is containeraized
    // API_URL: 'http://localhost:3005/'

    // Access hosted backend api from heroku
    API_URL: 'https://binarybackend.herokuapp.com/'
};