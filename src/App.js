// * @Author: Ismail Debele Asega
// * @Email: asega03@gmail.com
// * @LinkedIn: https://www.linkedin.com/in/asegaismail/
// * @Github: https://github.com/ismailasega
// * @GitLab: https://gitlab.com/asegaismail
// * @Tel: +256-784-491412 / +256-756-454376

import React from 'react';
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from "react-router-dom";

// View Pages
import DocumentUploadPage from './pages/uploadPage';

const App = () => {
  return (
    <Router basename="/">
      <Routes>
        <Route path='/' element={ <Navigate to="/documentUpload" /> } />
        <Route path='/documentUpload' element={<DocumentUploadPage/>} exact />
      </Routes>
    </Router>
  );
}

export default App;