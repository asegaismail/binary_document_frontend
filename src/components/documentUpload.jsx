// * @Author: Ismail Debele Asega
// * @Email: asega03@gmail.com
// * @LinkedIn: https://www.linkedin.com/in/asegaismail/
// * @Github: https://github.com/ismailasega
// * @GitLab: https://gitlab.com/asegaismail
// * @Tel: +256-784-491412 / +256-756-454376

import React, { useCallback, useState, useEffect } from 'react'
import { useDropzone } from 'react-dropzone'
import Typography from '@mui/material/Typography';
import LinearProgress from '@mui/material/LinearProgress';
import { Button, IconButton } from '@mui/material';
import UploadFileIcon from '@mui/icons-material/UploadFile';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import CancelOutlinedIcon from '@mui/icons-material/CancelOutlined';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import Input from '@mui/material/Input';
import Divider from '@mui/material/Divider';
import axios from 'axios';
import APIConfig from '../utils/apiConfig';
import DocumentView from '../components/documentView';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 900,
    height: 500,
    bgcolor: 'background.paper',
    boxShadow: 24,
    borderRadius: 3,
    p: 4,
};

const KycDocumentUpload = () => {

    // API config
    const API_URL = APIConfig.API_URL

    // Kyc document uploading loader and status
    const [isUploading, setUploading] = useState(false);
    const [uploadStatusSuccess, setUploadStatusSuccess] = useState();
    const [uploadStatusFailed, setUploadStatusFailed] = useState();

    // Modal
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    // Document Upload
    const onDrop = useCallback(async (acceptedFiles) => {

        const formData = new FormData();
        const [file] = acceptedFiles;
        formData.append("File", file);

        if (file || isDragAccept) {
            setUploading(true);
            await axios.post(API_URL + 'uploadDocument', formData,
                {
                    headers: { "Content-Type": "multipart/form-data" }
                })
                .then((res) => {
                    if (res.status === 200) {
                        setUploading(false);
                        handleClose();
                        getNewKycDocuments();
                        setUploadStatusSuccess('KYC Document Uploaded Successfully');
                        setTimeout(() => {
                            setUploadStatusSuccess(null);
                        }, 2500);
                    }
                })
                .catch((response) => {
                    setUploading(false);
                    setUploadStatusFailed('KYC Document Upload Failed');
                    setTimeout(() => {
                        setUploadStatusFailed(null);
                    }, 2500);
                    console.log(response);
                })
        }
    }, []);

    // Uploaded KYC view, array of docs and repsonse message
    const [isLoading, setIsLoading] = useState(false);
    const [kycDocuments, setKycDocuments] = useState([]);
    const [noDataResponseMessage, setNoDataResponseMessage] = useState('');

    // Get all kyc documents uploaded
    const getAllKycDocuments = (async () => {
        setIsLoading(true);
        try {
            await axios.get(API_URL + 'kycDocuments')
                .then((res) => {
                    if (res.data.status === 'Failed') {
                        setKycDocuments([]);
                        setNoDataResponseMessage(res.data.message)
                        setIsLoading(false);
                    } else {
                        let kycDocs = res.data.files;
                        let sortedList = kycDocs.sort((a, b) => a - b).reverse();
                        setKycDocuments(sortedList);
                        setIsLoading(false);
                    }
                })

        } catch (err) {
            console.log(err);
        }
    })

    useEffect(() => {
        getAllKycDocuments();
    }, [])

    // Fetching new kyc list after upload
    const getNewKycDocuments = (async () => {
        try {
            await axios.get(API_URL + 'kycDocuments')
                .then((res) => {
                    if (res.data.status === 'Failed') {
                        setKycDocuments([]);
                        setNoDataResponseMessage(res.data.message)
                    } else {
                        let kycDocs = res.data.files;
                        let sortedList = kycDocs.sort((a, b) => a - b).reverse()
                        setKycDocuments(sortedList);
                    }
                })
        } catch (err) {
            console.log(err);
        }
    })

    // Dropzone props
    const { getRootProps, getInputProps, isDragActive, isDragAccept } = useDropzone({
        onDrop,
        accept: {
            'image/jpg': ['.jpg',  '.png', '.pdf']
        }
    });

    return (
        <>
            <Button sx={{ marginTop: 3, alignSelf: 'flex-end', width: 180 }} size='small' color='primary' variant='contained' startIcon={< UploadFileIcon />} onClick={handleOpen}>
                New Document
            </Button>
            {uploadStatusSuccess && <Box sx={{ marginTop: 2, display: 'flex', flexDirection: 'row', fontSize: 14, fontWeight: 500, height: 40, bgcolor: '#F5FFFA', color: '#2E8B57', alignItems: 'center' }}><CheckCircleOutlineIcon sx={{ p: 1 }} /><Typography >{uploadStatusSuccess}</Typography></Box>}
            {uploadStatusFailed && <Box sx={{ marginTop: 2, display: 'flex', flexDirection: 'row', fontSize: 14, fontWeight: 500, height: 40, bgcolor: '#ffebeb', color: '#e21d1d', alignItems: 'center' }}><CancelOutlinedIcon sx={{ p: 1 }} /><Typography >{uploadStatusFailed}</Typography></Box>}
            {/* Document Upload Modal */}
            <Modal
                open={open}
                aria-labelledby='modal-modal-title'
                aria-describedby='modal-modal-description'
            >
                <Box sx={style}>
                    <Box sx={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }} >
                        <Typography id='modal-modal-title' variant='h6' component='h2'>
                            Upload Document
                        </Typography>
                        <IconButton onClick={handleClose}>
                            <CancelOutlinedIcon />
                        </IconButton>
                    </Box>
                    <Divider />
                    {isUploading ?
                        <Box sx={{ width: '100%', marginTop: 25 }}>
                            <Typography sx={{ fontSize: 14, fontWeight: 400, color: '#616161', textAlign: 'center' }}>Uploading...</Typography>
                            <LinearProgress />
                        </Box>
                        :
                        <div {...getRootProps()}>
                            <Box sx={{ marginTop: 5, height: 400, display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                                <Input {...getInputProps()} />
                            {isDragActive ? 
                                <Typography sx={{ fontSize: 28, fontWeight: 400, color: '#616161' }} >Drop a file here!</Typography>
                                        :
                                    <>  <Typography sx={{ fontSize: 16, fontWeight: 400, color: '#616161' }}>Drag and drop a file here</Typography>
                                        <Typography sx={{ fontSize: 14, fontWeight: 400, color: '#616161' }}>Or, click icon below to select a file from your device. Accepted files are png, jpg, pdf</Typography>
                                        <IconButton {...getRootProps()}>
                                            <UploadFileIcon />
                                        </IconButton>
                                    </>
                                }
                            </Box>
                        </div>
                    }
                </Box>
            </Modal>
            <DocumentView noDataResponseMessage={noDataResponseMessage} isLoading={isLoading} kycDocuments={kycDocuments} />
        </>
    );
}

export default KycDocumentUpload;