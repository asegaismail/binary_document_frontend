// * @Author: Ismail Debele Asega
// * @Email: asega03@gmail.com
// * @LinkedIn: https://www.linkedin.com/in/asegaismail/
// * @Github: https://github.com/ismailasega
// * @GitLab: https://gitlab.com/asegaismail
// * @Tel: +256-784-491412 / +256-756-454376

import React, { useState  } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import TablePagination from '@mui/material/TablePagination';
import Paper from '@mui/material/Paper';
import APIConfig from '../utils/apiConfig'
import moment from "moment";
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';

const KycDocumentView = (props) => {

    const { kycDocuments, isLoading, noDataResponseMessage } = props;

    const formatDate = (value) => {
        if (value) {
            return moment(String(value)).format('MMM Do YYYY, h:mm:ss a');
        }
    }
    // Pagination
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    return (
        <Paper sx={{ marginTop: 2, width: '100%', overflow: 'hidden' }}>
            <TableContainer sx={{ maxHeight: 440 }} >
                <Table stickyHeader aria-label="sticky table">
                    <TableHead>
                        <TableRow >
                            <TableCell sx={{ fontWeight: 900 }}>FileName</TableCell>
                            <TableCell sx={{ fontWeight: 900 }} align="right">contentType</TableCell>
                            <TableCell sx={{ fontWeight: 900 }} align="right">uploadDate</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {isLoading ?
                            <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                                <TableCell colSpan={12}>
                                    <Box sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                                        <CircularProgress />
                                        <Typography sx={{ marginTop: 1, fontSize: 14, fontWeight: 400, color: '#616161', textAlign: 'center' }}>Loading...</Typography>
                                    </Box>
                                </TableCell>
                            </TableRow>
                            :
                            null}
                        {!isLoading && kycDocuments.length <= 0 ?
                            <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                                <TableCell colSpan={12}>
                                    <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                        {noDataResponseMessage}
                                    </Box>
                                </TableCell>
                            </TableRow>
                            :
                            null}

                        {!isLoading && kycDocuments.length > 0 ?
                            <>
                                {kycDocuments
                                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                    .map((file, index) => (
                                        <TableRow
                                            key={index}
                                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                        >
                                            <TableCell component="th" scope="row">
                                                {file.filename}
                                            </TableCell>
                                            <TableCell align="right">{file.contentType}</TableCell>
                                            <TableCell align="right">{formatDate(file.uploadDate)}</TableCell>
                                        </TableRow>
                                    ))}
                            </>

                            : null}

                    </TableBody>

                </Table>
            </TableContainer>
            <TablePagination
                rowsPerPageOptions={[10, 25, 100]}
                component="div"
                count={kycDocuments.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
            />
        </Paper>
    )
}

export default KycDocumentView;