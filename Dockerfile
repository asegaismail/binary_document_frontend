
FROM node:14-alpine

# Creating app directory
WORKDIR /usr/src/ducumentUploadApp

# Copying both package.json AND package-lock.json
COPY package*.json ./

# Installing app dependencies
RUN npm install

# Bundling app source (Copy all files to enviroment)
COPY . .

# Exposing Application Port
EXPOSE 8080

# Command to run the application
CMD [ "npm", "run", "dev" ]