# Binary upload Frontend

- Create a document upload Drop-Zone using React Drop-Zone.
- Use MaterialUI for styling
- Allow the drop zone to accept only PDF, PNG and JPG files.

# Setting App on local machine
- Do a git clone: git@gitlab.com:asegaismail/binary_document_frontend.git

# Running application without docker
- run command: npm i <to install dependences>
- run command: npm run dev <to load application via broswer> 

# Running application via Docker container
- run command: docker build -f Dockerfile -t uploaddocument .
- run command: docker run -it -p 3001:8080 uploaddocument
- after container is up and running you can access application via: *** http://localhost:3001/ ***

# App build in
- React @18.2.0